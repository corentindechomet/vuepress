const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Documentation Front SSL',
  
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Configuration pour le déploiement
   */
  base: '/vuepress/',
  dest: 'public',

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: [
      {
        text: 'Guide',
        link: '/guide/',
      }
    ],
    sidebar: [
      {
        title: 'Vue js',
        children: [
          '/vue/components',
          '/vue/single-files-components',
          '/vue/hooks-and-lifecycles',
          '/vue/computed-vs-watchers',
          '/vue/methods',
          '/vue/directives',
          '/vue/props',
          '/vue/scss-scoped',
          '/vue/routing',
          '/vue/testing',
          '/vue/tools',
          '/vue/i18n',
          '/vue/good-practices'
        ]
      },
      {
        title: 'Nuxt',
        children: [
          '/nuxt/ssr',
          '/nuxt/hooks-and-lifecycles',
          '/nuxt/asyncdata-fetch',
          '/nuxt/deployment'
        ]
      },
      {
        title: 'VueX',
        children: [
          '/vuex/what-is-vuex',
          '/vuex/when-to-use-vuex',
          '/vuex/vuex-structure',
          '/vuex/state-mutation-action'
        ]
      },
      {
        title: 'Intégration web: HTML & CSS',
        children: [
          '/html-css/semantic',
          '/html-css/css',
          '/html-css/responsive',
          '/html-css/frameworks'
        ]
      },
      {
        title: 'SEO',
        children: [
          '/seo/metadatas',
          '/seo/keywords',
          '/seo/microformats',
          '/seo/microdatas'
        ]
      },
      {
        title: 'Accessibilité',
        children: [
          '/accessibility/tools',
          '/accessibility/rgaa',
          '/accessibility/guides'
        ]
      },
      {
        title: 'UX/UI',
        children: [
          '/ux-ui/colors',
          '/ux-ui/fonts',
          '/ux-ui/components-tips',
          '/ux-ui/icons',
          '/ux-ui/images',
          '/ux-ui/heuristics',
          '/ux-ui/ux-games',
          '/ux-ui/checklists',
          '/ux-ui/errors-management',
          '/ux-ui/ssl-books',
        ]
      }
    ]
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
