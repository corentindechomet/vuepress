---
home: true
heroImage: hero.png
tagline: Un simple wiki pour centraliser les connaissances front chez SSL
actionText: C'est parti ! →
actionLink: /guide/
features:
- title: Développement Front-end
  details: Vue.js, Nuxt, VueX !
- title: UX / UI
  details: Ressources, tips & tricks
- title: Et d'autres sujets !
  details: SEO, Accessibilité, ...
footer: Made by Corentin with ❤️
---
